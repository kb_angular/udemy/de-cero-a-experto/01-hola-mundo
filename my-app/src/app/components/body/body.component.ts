import { Component } from '@angular/core';
import { stripSummaryForJitNameSuffix } from '@angular/compiler/src/aot/util';

@Component ({
    selector: 'app-body',
    templateUrl: './body.component.html'
})

export class BodyComponent {

    mostrar = true;

    personajes: string[] = ['Spiderman', 'Venon', 'Dr. Octupus'];

    frase: any = {
        mensaje: 'Un gran poder requiere de una gran mensaje',
        autor: 'Ben Barquer'
    };

}
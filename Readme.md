# ANGULAR
    Una aplicacion de Angular esta basada en multiples componente. Un componentes son pequenas clases que cumplen tareas especificas.
        - Menu de navegacion
        - Barra Lateral
        - Demas paginas y sub paginas
        - pie de pagina o de aplicacion.
    
    * Angular app --> Uno o mas **Modulos**
    * Modulos     --> Uno o mas **componentes** o **servicios**
    * Componentes --> HTML + Class
    * Servicio    --> Logica del negocio
    * los modulos interactuan para renderizar la vista en el navegador.

##  Directivas estructurales
    *ngif   --> "TRUE"  "FALSE", es la que se encargar de mostrar y ocultar elementos HTML.

    *ngFor  --> Es la que se encargar de hacer repeticiones de nuestro elementos HTML.


## Creacion de la Aplicacion
    Angular no rescomienda utilizar Angular CLI

    ng new 'nombre'

## Para levantar nuestro Proyecto
    
    ng serve -p 4200 --> Es opcional -p que es el 
    Puerto.

    ng serve -o      --> levantar la aplicacion en la web

## Cargar un proycto, y no existe node_modules
    npm install


## Para crar un Component
    ng g c components/footer

    g --> generate
    c --> component
    components/footer --> path de la creacion

    



    




